## Self-made BSgenome Package for UCSC ponAbe2 Genome Assembly

The seed file is located at `inst/extdata/BSgenome.PAbelii.UCSC.ponAbe2-seed`

To install, use

```r
devtools::install_git('https://gitlab.com/Marlin-Na/BSgenome.PAbelii.UCSC.ponAbe2.git')
```

